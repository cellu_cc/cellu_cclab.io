module.exports = {
    title: '',
    description: 'Daniel Cellucci\'s Portfolio Site',
    dest: 'public',

  	themeConfig: {
  		navbar: true,
	    sidebar: [
	      ['/','Daniel Cellucci'],
	      ['MANIFESTO','Manifesto'],
	      ['PROJECTS', 'Projects'],
          ['PORTFOLIO', 'Portfolio'],
	      ['ABOUTME', 'About Me']
	    ]
  	}
}
