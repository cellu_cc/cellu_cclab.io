---
prev: false
next: false
---

## Embedded Systems

My work in embedded systems has spanned the full space of hardware development- everything from high-level application software to device driver development to board design and component selection. Here is a selection of some projects I have worked on.

### Distributed Sensing

![IMAGE](./media/portfolio/distributedsensing/nodesensorhardware.png)

This project involved the instrumentation of an aircraft with many low-cost pressure sensors in order to provide a full picture of the aircraft's lift profile in flight. Initial tests used a network of Atmel SAMD21 processors as the data collectors and routers. Subsequent work has focused on a wireless network of Nordic nrf52840s running OpenThread on RIOTOS.

My technical accomplishments include:

 1. Porting of the Nordic nrf52840 802.15.4 radio driver for the [Rust-based TockOS](https://github.com/tock/tock/pull/1237).
 2. Development of a [novel bootloader](https://gitlab.com/cellu_cc/Viral) for SAMD21 based on BOSSA that automates the process of Device Firmware Updates for wired networks of processors using a gossip-based algorithm.
 3. Designing a [distributed algorithm for lift estimation](https://ieeexplore.ieee.org/abstract/document/8593664) intended to run directly on the network processors. 

### The Multi-Orthogonal Jaunting Robot (MOJO)

![IMAGE](./media/portfolio/robotics/mojo.png)

This project involved the development of a minimum degree-of-freedom robotic platform capable of locomoting through a three-dimensional periodic lattice. Since this robot was untethered, I used an Atmel SAMR21 MCU to communicate wirelessly with a user, control the actuators, and monitor robot state. I also was able to test this robot in a parabolic flight, requiring that I provide responsive, reliable code in order to facilitate the tests.

![IMAGE](./media/portfolio/robotics/mojomoving.png)

My technical accomplishments include:

 1. Designing a high-amperage (\~2A max) power system that provided regulated power from a LiPo battery to five servos and monitored consumption and state.
 2. Designing a novel board layout that incorporated board features into the robot's mechanical system in order to minimize weight
 3. Developing a Interface Control Document and testing interface in order to facilitate locomotion experiments on board a parabolic flight.

### OuroboroSat

![IMAGE](./media/portfolio/robotics/ouroborosat.png)

This project was my introduction to circuit design, and involved a modular Cubesat that could be constructed in minutes and was capable of providing power sharing between modules. I demonstrated this functionality on a sounding rocket flight. 

My technical accomplishments include:

 1. Design of a multi-source MPPT circuit capable of either pulling charge from a set of solar panels or from a neighboring module.
 2. Implementation of a novel routing algorithm called the Asynchronous Packet Automaton (developed at MIT's Center for Bits and Atoms) that uses local addressing rather than global addressing.

## Data Analysis

Many of my projects involved many hundreds of sensors collecting data that later needed to be analyzed for. 

For this work, I developed the following skills:

 1. Extensive knowledge of the management and manipulation of large datasets using **CDF** and **Pickle**.
 2. Analysis of these datasets using libraries like **Pandas**, **Numpy**, and **SciPy**

### Distributed Spacecraft Autonomy

This work involved a survey of data collected from the ESA's SWARM mission. All three satellites were equipped with a GPS receiver capable of estimating the plasma density of the space through which the broadcast radio signals passed. These plasma density data directly informed the trade studies used to assess satellite instrument selection, desired algorithm performance, and crosslink radio requirements. 

 1. Development of automated scripts for downloading and extracting satellite telemetry data from the ESA data servers.
 2. Analysis of the collected telemetry for typical operating states and possible plasma features, as well as their spatial and temporal properties.

### Distributed Sensing

This work involved both the raw data collection scheme, as well as the design of the datasets and processing pipeline that allowed my collaborators and I to quickly iterate on the distributed algorithms we wanted to test.

 1. Development of a [low-footprint data collection scheme](https://gitlab.com/cellu_cc/MADCATV1-14x22-RawData) capable of running on resource constrained hardware but still capable of operating at the desired data rate (100 Hz).
 2. Documentation of a [data pipeline](https://gitlab.com/cellu_cc/MADCATV1-14x22-Analysis) that transformed raw data into a consistent and tagged volume that could then be used to test candidate distributed estimation algorithms. 

## Mathematics

My analytical work in mathematics has mainly focused on the problem of structural rigidity in three-dimensional periodic lattices. In particular, I have performed extensive work analyzing a class of lattices that

To this end, I have employed the following skills:

 * Group theory analysis of periodic structures, such as the work developed by [Guest and Fowler](https://royalsocietypublishing.org/doi/full/10.1098/rsta.2012.0029), as well as the matrix-based techniques developed by [Vigliotti and Pasini](https://www.sciencedirect.com/science/article/pii/S0045782512000941).
 * Development of [custom finite element codes](https://gitlab.com/cellu_cc/pfea) that simplify much of the process of generating heterogenous cellular solids lattices for simulation.
 * Development of [custom geometry software](https://gitlab.com/cellu_cc/latticegen) that generates watertight STL meshes of arbitrary cellular solids lattices, for manufacturing using 3D printing. 