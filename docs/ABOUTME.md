---
prev: false
next: false
---

# About Me

I started out life wanting to be a physicist, but spent way more time than was reasonable or necessary in my University's sculpture department learning how to run a CNC machine and program an Arduino. 
After graduating, I realized that engineering was the weighted average between what I was doing in science and in art, and I've been doing that ever since, first at Cornell University, and then at NASA Ames Research Center. 

I like to make things, and I especially like learning how to make new things. When I'm not doing that, you can find me baking bread or biking. 

## Skills

### Fabrication Skills

| Name       | Description | Proficiency (out of 5)  |
| -------------: |:----------|:-----|
| Additive Manufacturing | FDM, SLA, multi-material | :printer: :printer: :printer: :printer: :printer: |
| Circuit Design | Power electronics, RF circuits, motor controllers | :robot: :robot: :robot: :robot: :robot: 
| Machining      | CNC/Manual, Mill/Lathe, Plastic/Metal | :triangular_ruler: :triangular_ruler: :triangular_ruler: :triangular_ruler:  | 
| Laser Cutting    | 100 kW - 1 MW      | :fire: :fire: :fire: :fire:  |
| Injection Molding | Plastics, fiber-reinforced | :balloon:  :balloon:  :balloon: |
| Woodworking | band saw, table saw, joinery     |:deciduous_tree: :deciduous_tree: :deciduous_tree:|
  
### Programming Skills

| Name       | Application Area | Proficiency (out of 5)  |
| -------------: |:----------|:-----|
| C | Embedded SDKs | :wrench: :wrench: :wrench: :wrench: |
| Python | iPython, numpy, pandas | :snake: :snake: :snake: :snake: |
| C++ | application code | :hammer_and_wrench: :hammer_and_wrench:|
| Rust | microcontroller kernel | :gear: :gear: :gear:|


### Testing & Characterization Skills

| Name       | Application Area | Proficiency (out of 5)  |
| -------------: |:----------|:-----|
| Universal Testing Machines| Tensile, Compressive Modulus | :clamp: :clamp: :clamp: :clamp: |
| Space Flight Certification  | Vibration, Thermal/Vacuum, Shock | :artificial_satellite: :artificial_satellite: :artificial_satellite: |
| Parabolic Flight Testing |  | :nauseated_face: :nauseated_face:  |

