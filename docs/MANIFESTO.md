---
prev: false
next: false
---

# Manifesto

The best summary of my work is that it is an exploration of the engineering behind making a SPIME.

## What is a SPIME? 
Also known as a SPace-tIME object, it is a design concept originally introduced by [Bruce Sterling](https://mitpress.mit.edu/books/shaping-things). 
A SPIME spends most of its life as uninstantiated data on a server somewhere else. 
When it is needed, a machine manufactures a copy of it, embeds it with sensors, and provides it to the user who requested it. 
The SPIME then lives its life and logs every moment of its use. 
When it is no longer needed, the user disposes of it. 
Instead of the SPIME going to a landfill, however, the materials are decomposed into their original constituent parts, ready to be assembled into the next object, and the collected data are sent back to the server hosting the design. 
This server then combs through the data for insights that can be used to improve the design. The next time the SPIME is requested, it is manufactured with these improvements.

![SPIME SHOES](./media/introduction/Spime_shoes.png)
_Figure courtesy of [Michelle Fong](http://michellefong.co/)_

### For Example: A Pair of SPIME Shoes

For example, consider a pair of SPIME shoes. 
These shoes first appear as a Computer Aided Design (CAD) model in an interface. 
They will be customizable: color, texture, lace type, et cetera  are all options. 
The one option that is not present is the size- the user instead uses a CAD model of each of their feet that, when their design is ready, a printer uses to make a pair of shoes that fit better than any standard size could.
The opportunities and challenges of the manufacturing infrastructure required to produce such a customized shoe have been well described, usually in the context of [additive manufacturing](https://www.amazon.com/Fabricated-New-World-3D-Printing/dp/1118350634). 
Sterling himself refers to these endlessly customizable objects as GIZMOs, a sort of predecessor to the SPIME that focuses on extensibility at the cost of stability. 
However, there are two critical capabilities that make a SPIME distinct from a GIZMO: the data that the shoes collect and the ability to seamlessly recycle the shoes back into the matter stream once their use has been completed. 
The motivation for the former capability is functional, the latter is existential. 

#### The Functional Motivation: Better Objects

With the former capability, the data collected during the SPIME's lifetime completes the picture for a GIZMO by providing the peripheral information that is not available without testing in the operating environment. 
Perhaps the user tends to walk on the balls of their feet, so the shoe's sole wears there first. 
A SPIME shoe notices this, and the next shoe will have harder material in these worn spots. 
Perhaps the user just moved to a town like Ithaca that likes to salt every available surface when it snows. 
A SPIME shoe learns this, and the next shoe uses a material that is resistant to salt.
These kinds of externalities might be predictable with a robust enough model, but with seamless manufacturing and recycling, it is easier to build the object, use it, and derive the lessons learned from the results than it is to try to predict all of these scenarios beforehand.

#### The Existential Motivation: A 'Better' Civilization


With the latter capability, design for recycling is the basis of Sterling's thesis on SPIMEs, and the [Viridian Design Movement](http://www.viridiandesign.org/manifesto.html) for which Shaping Things was one of many manifestos. 
Sterling was not just concerned with the SPIME as an engineering challenge, Sterling wanted to imagine the kind of society that would be built on SPIMEs. 
Foremost, he theorized, a SPIME society would be concerned with its future, and would try to act in a way that maximized its longevity. 
From this perspective, a manufacturing system that pollutes the Earth, produces large amounts of CO2, and sends the vast majority of its products to a slow death in a landfill is an existential threat that must be addressed. 
Sterling's theory is not so much that this society is inevitable, but that this society is, through some kind of natural selection, the only one guaranteed to survive such a self-imposed threat. 
That is, either modern society adopts the SPIME view or it consumes itself.

The critical insight for Sterling is we, as humans, already recycle and already iterate. 
We do these things poorly, slowly, and at great expense. 
Getting an improved pair of shoes is an intellectual labor that the user performs by sifting through the avalanche of products that companies produce in order to meet every possible demand as quickly as it is expressed. 
Since we are categorically bad at cradle-to-grave manufacturing, the vast majority of these products end up, unused, in the landfill.

### SPIMES to the Rescue

SPIMEs were proposed to solve both of these problems simultaneously. 
They will be adopted, according to Sterling's and the Viridian Design Movement's thesis, because they save the user and the producer time, money, and effort. 
SPIMEs will save the user effort because the product is a distillation of the user's needs and desires into a single, customizable, optimal design, instead of a vast range of static, half-useful products that need to be laboriously sought out, tried on, and agonized over before becoming a part of the user's life. 
SPIMEs will save the producer effort because they will no longer have to deal with stock or sales or clearance racks. 
Most importantly, however, both the user and the producer will make a self-interested decision based on convenience, and, in doing so, will unintentionally save the planet from being buried under a layer of not-quite-fashionable trash.

However, the reality is not so frictionless as Sterling suggests- it is not simply sufficient to suggest this system to the existing economic order and expect it to be adopted wholesale because it is rationally better. 
In addition to the variety of social impediments, there are numerous technologies that must first be developed in order to enable such a means of production. 
These technologies are necessary for the SPIME ecosystem but not exclusive to it- they can be employed in a variety of ways, one of which may be conducive to creating SPIMEs.

## The SPIME and Aerospace 

My work begins with the assumption that the aerospace industry is the ideal place for the development of the technologies that enable SPIMEs. 
In particular, SPIMEs meet two critical needs of the aerospace industry.
The first need concerns increased operational efficiency. 
Billions of dollars are spent every year to improve existing aircraft designs- performance increases of even a few percent can save millions of liters of fuel and thousands of metric tons of CO2. 
The data that SPIMES provide into the details of their use can form a critical component of aircraft design, providing the sort of insights that resulted in scalloped nacelles and wingtips, without the massive investments in research infrastructure required to manually discover these improvement.
The second need concerns material reuse and reconfigurability.
When a plane is decomissioned, it sits in a graveyard in the Mojave until it can be manually disassembled.
Multi-stage spacecraft discard the expended stages, leaving them to either drift for eons or (more optimistically) fall to Earth, burning up in the process.
The materials that compose these artifacts are usually the state of the art when they are built, representing the most advanced, energy-intensive, and expensive components of their time.
However, when the artifact's useful lifetime has been completed, in aerospace, it often is not or cannot be disassembled.

![SPIME AIRPLANE](./media/introduction/Plane_Hires.png)
_Figure courtesy of [Michelle Fong](http://michellefong.co/)_

What does an aircraft or spacecraft SPIME look like? 
What technologies are necessary to make these SPIMEs a reality?
These are the questions that my work attempts to answer.
The vehicle through which this dissertation explores these questions is an approach called a _circular manufacturing system_. 
A _circular manufacturing system_ provides the infrastructure possible to realize the essential qualities of a SPIME. 
It contains both the ability to produce an object and disassemble this object.
It also has the ability to augment the object with the sensors necessary to measure the performance of the object being produced, and read this information at the end of the object's life. 
This system can then comb these data for the kinds of insights necessary to improve the object's design.