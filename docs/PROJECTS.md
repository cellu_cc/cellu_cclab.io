---
prev: false
next: false
---

## Distributed Sensing

![IMAGE](./media/portfolio/distributedsensing/madcat_tunnel.jpg)

This work shows that a distributed system composed of many small, imprecise MEMS pressure sensors can provide an unprecedented level of detail into an aircraft's aerodynamic state. The system that provides these data is lightweight and responsive, meaning it can be integrated into both test articles and vehicles, potentially providing these vehicles with the insights necessary for provably safe autopilots, performance-optimizing adaptive controllers, and other cool things. 

![IMAGE](./media/portfolio/distributedsensing/nodesensorhardware.png)

A bit further [down the road](./MANIFESTO.md#the-spime-and-aerospace), the insights from these sensor systems can form the basis for SPIMEcraft, self-refining aircraft that can suggest design improvements that improve their performance and safety. Think of an embedded intelligence that automatically discovers [raked wingtips](https://en.wikipedia.org/wiki/Wingtip_device) or [scalloped nacelles](https://www.nasa.gov/topics/aeronautics/features/bridges_chevron_events.html), without the millions of dollars in test equipment and thousands of hours of testing that's required today. 

### My Contributions
  * designing the [sensor boards](https://gitlab.com/cellu_cc/Sensor-Sticker/tree/master/Skin-Sensor)
  * managing the hardware integration into the MADCAT wing
  * writing the firmware for collecting the data and sending it to a central source.
  * writing the [software](https://gitlab.com/cellu_cc/MADCATV1-14x22-Analysis) for analyzing the data
  * developing a novel algorithm (with [Nick Cramer](https://www.linkedin.com/in/nick-cramer-87a9232b)) for intelligently fusing the sensor data into a single estimate for lift

### Publications
   1. **Cellucci, D.**, Cramer, N., & Swei, S. S. M. (2018, October). Distributed Pressure Sensing for Enabling Self-Aware Autonomous Aerial Vehicles. In 2018 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS) (pp. 6769-6775). IEEE.
   2. Cramer, N., **Cellucci, D. W.**, Formoso, O. B., Gregg, C. E., Jenett, B. E., Kim, J. H., ... & Cheung, K. C. (2019). Elastic Shape Morphing of Ultralight Structures by Programmable Assembly. Smart Materials and Structures.

### Funding
This work was initially funded by the Mission Adaptive Digital Composites Aerospace Technologies (MADCAT) Project, which was a part of Convergent Aeronautics Systems Program in NASA's Aeronautics Mission Directorate. 

## Spacecraft Autonomy

![IMAGE](./media/portfolio/distributedautonomy/ionosphere.jpg)

I am currently the Instrumentation Lead on a multi-million dollar flight project that will demonstrate distributed consensus between several satellites flying together Low Earth Orbit. My responsibilities include coordination with members of the geophysics community to produce a use-case that showcases these algorithms using the on-board dual-band GPS receiver as an instrument. 

![IMAGE](./media/portfolio/distributedautonomy/SpacecraftAutonomyUseCase.png)

In the proposed use-case, each satellite will use the phase delay between the L1 and L2 bands of the GPS receiver to estimate the total electron content of the topside ionosphere. The satellites will coordinate with one another to identify phenomena such as polar patches or equatorial bubbles (depending on the location of the orbits and time of launch). Use of this group consensus will possibly reduce data volumes without compromising data quality, since satellites 

A bit further down the road, the work being developed here will be useful for spacecraft swarms with many tens to hundreds of satellites, operating in locations with severe bandwidth constraints when trying to communicate with Earth. This includes proposed missions to perform large scale imaging of the magnetosphere, as well as constellations that provide precise navigation and timing (PNT) on other bodies like the Moon and Mars.

### My Contributions
  * formulating a use case that utilizes existing on-board instruments to demonstrate autonomous coordination
  * communicating with science subject matter experts to validate and refine the formulated use case
  * identifying potential scenarios and features to which the spacecraft can dynamically respond
  * characterizing the phenomena in these scenarios and estimating their expected behavior
  * developing a simulator that provides a scalable framework for testing autonomy algorithms for an arbitrary number of coordinating spacecraft

### Awards and Accolades
**I received the NASA Spotlight Award** "For superior personal impact in steering technology development and experiments for the Distributed Spacecraft Autonomy project with scientific community engagement". 

### Funding
This work is funded by the Game Changing Development Program in NASA's Space Technology Mission Directorate.

## Periodic Lattices

![IMAGE](./media/portfolio/cellularsolids/Specimen_Design.png)

This work involves the discovery of a novel class of open-cell cellular solids derived from Triply Periodic Minimal Surfaces. 
Cellular solids are pretty interesting because you can relate their material performance to their density. 
This tuning lets you create materials with unprecedented performance, such as stiffness per unit mass.

![IMAGE](./media/portfolio/cellularsolids/CellularSolids.jpeg)

The cellular solids I discovered have the unique property of completely flat nodal connections. 
This properties allows the lattices to be broken into pieces and then assembled into arbitrary geometries by robots. Additionally, with some geometries, these locally flat connections can produce a tensegrity-like effect, where inducing a small pre-stress can produce a stiffer structure. 

### My Contributions
   * developing custom finite element modelling software for directly analying cellular solids lattices.
   * developing software that generates watertight STLs of arbitrary lattice geometries for experimental testing
   * performing theoretical analysis of these lattices using a [group theory extension of Maxwell's rigidity criterion](https://royalsocietypublishing.org/doi/full/10.1098/rsta.2012.0029).

## Robotics

### Multi-Orthongonal Jaunting rObot (MOJO)
![IMAGE](./media/portfolio/robotics/mojo.png)
I designed the electronics, actuator system, and firmware for a robot that can locomote in 3-dimensions inside a cellular solids lattice. 
This robot was small enough to fit inside a 3" pitch Cuboct Lattice.
I even got to operate this robot in an untethered locomotion test in zero gravity!

### Ribosomal Robotics
![IMAGE](./media/portfolio/robotics/walker_cropped.png)
I also built a machine that can custom manufacture automata from a strand of undifferentiated feed material. It was a finalist for the **Japan Toy Culture Foundation's Amusement Culture Award for Best Toy** at IROS 2017. This is the only award I ever want to win.


