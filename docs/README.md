---
prev: false
next: false
---

# Hello!

<img src="./media/me.jpg" alt="Me" width="500"/>

My name is Daniel Cellucci. I am a research technologist in the Advanced Manufacturing Division at NASA Ames Research Center, in the Bay Area. 

This is where I have some [writings about my research](MANIFESTO.html) and some documentation of a few projects I have worked on.

For a high-level description of my current work, see below. For a more technical description of my engineering contributions, check out [my portfolio](PORTFOLIO.html).

## Work

### Distributed State Estimation

![IMAGE](./media/portfolio/distributedsensing/madcat_tunnel.jpg)

This work shows that a distributed system composed of many small, imprecise MEMS pressure sensors can provide an unprecedented level of detail into an aircraft's aerodynamic state. The system that provides these data is lightweight and responsive, meaning it can be integrated into both test articles and vehicles, potentially providing these vehicles with the insights necessary for provably safe autopilots, performance-optimizing adaptive controllers, and other cool things. [READ MORE](PORTFOLIO.html#distributed-sensing)

### Spacecraft Autonomy

![IMAGE](./media/portfolio/distributedautonomy/ionosphere.jpg)

I am currently the Instrumentation Lead on a multi-million dollar flight project that will demonstrate distributed consensus between several satellites flying together Low Earth Orbit. My responsibilities include coordination with members of the geophysics community to produce a use-case that showcases these algorithms using the on-board dual-band GPS receiver as an instrument. [READ MORE](PORTFOLIO.html#spacecraft-autonomy) 

### Periodic Lattices

![IMAGE](./media/portfolio/cellularsolids/Specimen_Design.png)

As part of my dissertation, I discovered a novel class of open-cell cellular solids derived from Triply Periodic Minimal Surfaces. 
Cellular solids are a useful approach to materials design because they connect the mechanical performance of a material with the density of a lattice composed of that material. 
This connection enables the creation of materials with unprecedented performance, such as stiffness per unit mass.
I used symmetry-extended counting rules to predict the behavior of the lattices I discovered, and verified these predictions with experimental testing. [READ MORE](PORTFOLIO.html#periodic-lattices)

### Robotics

#### Multi-Orthongonal Jaunting rObot (MOJO)
![IMAGE](./media/portfolio/robotics/mojo.png)
I designed the electronics, actuator system, and firmware for a robot that can locomote in 3-dimensions inside a cellular solids lattice. 
This robot was small enough to fit inside a 3" pitch Cuboct Lattice.
I even got to operate this robot in an untethered locomotion test in zero gravity!

#### Ribosomal Robotics
![IMAGE](./media/portfolio/robotics/walker_cropped.png)
I also built a machine that can custom manufacture automata from a strand of undifferentiated feed material. It was a finalist for the **Japan Toy Culture Foundation's Amusement Culture Award for Best Toy** at IROS 2017. This is the only award I ever want to win.


